<?php
//équivalent d'un : import {First} from 'src/Classes';
use App\Classes\First;

require 'vendor/autoload.php';

//let maVariable = 'bloup';
$maVariable = 'bloup';
//le number en php est séparé en integer (entier) et float (virgule)
$nbEntier = 1;
$nbVirgule = 1.2;
$booleen = true;
$tableau = ['ga','zo','bu','meu'];

if($maVariable == 'bloup') {
    echo 'maVariable contient bloup';
} elseif($booleen) {
    echo 'maVariable ne contient pas bloup mais booleen est true';
}
//for(let i = 0; i < 10; i++) {}
for ($i=0; $i < 10; $i++) { 
    echo $i;
}
// for(let iterator of tableau) {}
foreach ($tableau as $index => $iterator) {
    echo $iterator;
}

$instance = new First('truc');
$instance->setProp3('bloup');

// echo $maVariable;

// echo makePara('bloup');
/**
 * En PHP on peut typer statiquement les arguments et 
 * le retour des fonctions. Un argument typé de la sorte
 * devra FORCEMENT être du type voulu sous peine de plantage
 * du PHP. J'encourage à le faire !
 */
function makePara(string $text): string {
    //return '<p>' + text + '</p>';
    return '<p>' . $text . '</p>';
}