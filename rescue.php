<?php

require 'vendor/autoload.php';

use App\DogRescue\Dog;
use App\DogRescue\Shelter;

/**
 * On fait une instance de chien en lui donnant en premier
 * un nom et en deuxième une instance de DateTime qu'on
 * crée à la volée
 */
$dog = new Dog('Fido', new \DateTime('2014-10-03'));
$dog2 = new Dog('Rex', new \DateTime('2016-11-10'));
//On fait un echo du retour de la méthode draw() du chien
// echo $dog->draw();

// var_dump($dog);

// echo $dog->getName();
$shelter = new Shelter();
$shelter->addDog($dog);
$shelter->addDog($dog2);

echo $shelter->draw();
