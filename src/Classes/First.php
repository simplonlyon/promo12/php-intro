<?php
/**
 * Le namespace est un peu l'équivalent en PHP du JS modulaire
 * Il doit être la première chose déclarée dans une classe PHP.
 * Le App\ correspond au dossier src/ car c'est ce que l'on
 * a indiqué dans le fichier composer.json au niveau du autoload
 * La suite du namespace correspond aux dossier et sous dossier 
 * dans lequel se trouve le fichier contenant la classe.
 * Ici, notre classe étant dans src/Classes, le namespace devient App\Classes.
 * Attention, en PHP il faut que le nom du dossier corresponde
 * exactement au namespace et que le nom de la classe corresponde exactement
 * au nom du fichier (case sensitive)
 */
namespace App\Classes;

class First {
    /**
     * En PHP, on déclare les propriétés directement dans
     * le corps de la classe, avec juste le nom de la propriété
     * précédé de sa visibilité.
     * Les différentes visibilités possibles sont :
     * - public : accessible depuis la classe et l'extérieur de celle ci
     * - private : accessible uniquement à l'intérieur de la classe elle même
     * - protected : accessible uniquement à l'intérieur de la classe elle même et de ses enfants par héritage
     */
    public $prop1;
    protected $prop2;
    private $prop3;

    /**
     * Le __construct php sert comme en JS à assigner
     * des valeurs aux propriétés de la classe à la différence
     * qu'on peut typer les arguments du constructeur
     */
    public function __construct(string $prop3) {//équivalent du constructor(){} en JS
        $this->prop1 = 'bloup';
        $this->prop2 = 'blip';
        $this->prop3 = $prop3;
    }
    

    public function setProp3(string $prop3):void {
        $this->prop3 = $prop3;
    }
}