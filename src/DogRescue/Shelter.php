<?php

namespace App\DogRescue;

class Shelter {
    /**
     * @var Dog[] la liste des chiens
     */
    private $list;

    public function __construct() {
        $this->list = [];
    }
    /**
     * Méthode permettant d'ajouter un chien à la liste
     */
    public function addDog(Dog $dog): void {
        $this->list[] = $dog;
        // array_push($this->list, $dog);
    }

    public function draw(): string {
        $html = '<section class="shelter">';
        foreach ($this->list as $dog) {
            $html .= $dog->draw();
        }

        $html .= '</section>';
        return $html;
    }
}



/*

manière alternative permettant d'ajouter plusieurs chien
à la fois comme ça : 
$instance->addDog($dog,$dog2, $dog3);

    public function addDog(Dog ...$dog): void {
        
        array_push($this->list, ...$dog);
    }
    */